'use strict';

var _electron = require('electron');

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _nodeNotifier = require('node-notifier');

var _nodeNotifier2 = _interopRequireDefault(_nodeNotifier);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var win = void 0;

function createWindow() {
    win = new _electron.BrowserWindow({
        width: 800,
        height: 600
    });
    win.loadURL(_url2.default.format({
        pathname: _path2.default.join(__dirname, "index.html"),
        protocol: "file:",
        slashes: true
    }));
    win.openDevTools();
    win.on('closed', function () {
        return win = null;
    });
    _nodeNotifier2.default.notify({
        title: 'My notification',
        message: 'Hello, there!',
        icon: _path2.default.join(__dirname, 'res', 'speaker.ico'),
        sound: true,
        wait: true
    }, function (err, res) {});
}

_electron.app.on('ready', createWindow);
_electron.app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') _electron.app.quit();
});
_electron.app.on('activate', function () {
    if (win === null) createWindow();
});
//# sourceMappingURL=index.js.map
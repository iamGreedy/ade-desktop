"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _winreg = require("winreg");

var _winreg2 = _interopRequireDefault(_winreg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Audio = function () {
    function Audio() {
        _classCallCheck(this, Audio);

        this._leg = new _winreg2.default({
            hive: _winreg2.default.HKLM,
            key: '\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\MMDevices\\Audio\\Render'
        });
        this.__lastUpdate = new Data();
        this.__devices = [];
    }

    _createClass(Audio, [{
        key: "update",
        value: function update() {
            this.__lastUpdate = new Data();
            this._leg.keys(function (err, items) {
                console.log(items);
            });
        }
    }]);

    return Audio;
}();

module.exports.Audio = Audio;
//# sourceMappingURL=Audio.js.map
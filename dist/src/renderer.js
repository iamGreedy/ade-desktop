"use strict";

var _index = require("./control/index");

var _index2 = _interopRequireDefault(_index);

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _nodeNotifier = require("node-notifier");

var _nodeNotifier2 = _interopRequireDefault(_nodeNotifier);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _jquery2.default)(document).ready(function () {
    (0, _jquery2.default)("#ALL").click(function () {
        // console.log(__dirname)
        _nodeNotifier2.default.notify({
            title: 'My notification',
            message: 'Hello, there!'
        });
    });
});
//# sourceMappingURL=renderer.js.map
import {app, BrowserWindow} from "electron"
import path from 'path'
import url from 'url'
import not from "node-notifier"



let win


function createWindow() {
    win = new BrowserWindow({
        width: 800,
        height: 600,
    })
    win.loadURL(url.format({
        pathname: path.join(__dirname, "index.html"),
        protocol: "file:",
        slashes: true,
    }))
    win.openDevTools()
    win.on('closed', () => win = null)
    not.notify({
        title: 'My notification',
        message: 'Hello, there!',
        icon: path.join(__dirname, 'res', 'speaker.ico'),
        sound: true,
        wait: true,
    }, function (err, res) {

    })
}

app.on('ready', createWindow)
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})
app.on('activate', () => {
    if (win === null) createWindow()
})

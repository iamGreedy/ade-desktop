import Reg from "winreg"

class Audio {
    constructor() {
        this._leg = new Reg({
            hive: Reg.HKLM,
            key: '\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\MMDevices\\Audio\\Render'
        })
        this.__lastUpdate = new Data()
        this.__devices = []
    }

    update()
    {
        this.__lastUpdate = new Data()
        this._leg.keys(((err, items) => {
            console.log(items)
        }))
    }
}
module.exports.Audio = Audio